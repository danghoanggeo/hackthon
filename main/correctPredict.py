from PIL import Image, ImageFile
from sys import exit, stderr
from os.path import getsize, isfile, isdir, join
from os import remove, rename, walk, stat
from stat import S_IWRITE
from shutil import move
from argparse import ArgumentParser
from abc import ABCMeta, abstractmethod
from resizeimage import resizeimage
import numpy as np
import re

DATA_PATH = '../predict/p1'
SAVE_PATH_UPDATE_PREDICT = "../temp/update_predict.txt"
PREDICT_FILE_PATH = "../predict_data_text"
SAVE_PATH_ALL_PREDICT = "../temp/all_predict.txt"
SAVE_PATH_CORRECT_PREDICT = "../temp/correct_predict.txt"

def get_date_from_image_name(str):
    if(str =="Sun"):
        return "0"
    elif str == "Mon":
        return "1"
    elif str == "Tue":
        return "2"
    elif str == "Wed":
        return "3"
    elif str =="Thu":
        return "4"
    elif str == "Fri":
        return "5"
    else: return "6"

def recorrectPredictedFromDir( path):
    """Write a correct predicted from images name to file"""

    fileN = []
    imgArrLs = []
    for root, dirs, files in walk(path):
        for file in files:
            # Check file extensions against allowed list
            matches = True
            if matches:
                # File has eligible extension, so process
                label = file.split('.')[0]
                place_date = label.split('-')[0]
                evaluate = int(label.split('-')[1]) - 1
                place = place_date.split('?')[0]
                date_time = place_date.split('?')[1]
                day = get_date_from_image_name(date_time.split(':')[0])
                hour = int(date_time.split(':')[1])
                minute =  int(date_time.split(':')[2])
                #print(label.split('-')[1])
                fileN.append([place,day,hour,minute,evaluate])
    labelsName = np.asarray(fileN)
    np.savetxt(SAVE_PATH_UPDATE_PREDICT,labelsName,fmt="%s",delimiter=" ")

def get_date_from_predict_file(str):
    minute = int(str.split('.')[1])
    day_hour = str.split('.')[0]
    day = day_hour[:1]
    hour = int(day_hour[1:])
    return day,hour,minute

def readAllPredictFileText( path):
    """Write a correct predicted from images name to file"""
    fileN = []
    imgArrLs = []
    for root, dirs, files in walk(path):
        for file in files:
            # Check file extensions against allowed list
            matches = True
            if matches:
                # File has eligible extension, so process
                fullpath = join(root, file)
                arrpredict = np.loadtxt(fullpath)
                for arrvalue in arrpredict:
                    place = str(int(arrvalue[0]))
                    day,hour,minute = get_date_from_predict_file(str(arrvalue[1]))
                    evaluate = int(arrvalue[2]) - 1
                    fileN.append([place,day,hour,minute,evaluate])

    labelsName = np.asarray(fileN)
    np.savetxt(SAVE_PATH_ALL_PREDICT,labelsName,fmt="%s",delimiter=" ")


def correct_wrong_predict():
    allPredict = np.loadtxt(SAVE_PATH_ALL_PREDICT)
    print("len all predict: %d"%(len(allPredict)))
    arr = {}
    for item in allPredict:
        keys = "%s-%s-%s-%s"%(str(int(item[0])),str(int(item[1])),str(int(item[2])),str(int(item[3])))
        if(~(keys in arr)):
            arr[keys] = int(item[4])
        else:
            print("Same key: %s value: %d"%(keys,int(item[4])))
    print("len dict predict: %d"%(len(arr)))
    updatePredict = np.loadtxt(SAVE_PATH_UPDATE_PREDICT)
    i = 0
    for item in updatePredict:
        keys = "%s-%s-%s-%s"%(str(int(item[0])),str(int(item[1])),str(int(item[2])),str(int(item[3])))
        arr[keys] = int(item[4])
        i += 1
    print("len correct predict: %d"%(i))
    newArr = []
    for key in arr:
        p_d_h_m = key.split('-')
        tem = [p_d_h_m[0],p_d_h_m[1],p_d_h_m[2],p_d_h_m[3],arr[key]]
        newArr.append(tem)
    labelsName = np.asarray(newArr)
    np.savetxt(SAVE_PATH_CORRECT_PREDICT,labelsName,fmt="%s",delimiter=" ")
    print("Completed")

if __name__ == "__main__":
    if isfile(DATA_PATH):
        processfile(DATA_PATH)
    elif isdir(DATA_PATH):
        recorrectPredictedFromDir(DATA_PATH)
        readAllPredictFileText(PREDICT_FILE_PATH)
        correct_wrong_predict()
    else:
        stderr.write('Invalid path "' + filename + '"\n')
        exit(1)
