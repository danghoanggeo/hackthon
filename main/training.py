from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from PIL import Image
import math
from io import BytesIO
import time
import urllib
import requests
import json
import re


# Set Eager API
tf.enable_eager_execution()
tfe = tf.contrib.eager

TRAIN_WEIGHT_PATH = "../model/1712_weigth_group1_day"
load_weight_path = "../model/1712_weights170+1e-05#768-512-256#92.h5"
TRAIN_DATA_PATH = '../dataset/1712_group1_day_data.txt'  #../bk_00/bk_v1/image_processing/train_data.txt
TRAIN_LABES_PATH = '../dataset/1712_group1_day_labes.txt'    #../bk_00/bk_v1/image_processing/
TEST_DATA_PATH = '../dataset/597_test_data.txt'
TEST_LABES_PAHT = '../dataset/597_test_labes.txt'
FIGURE_SAVE_PATH = '../figureSave/'
PREFIX_SAVE_IMAGE_PATH = '../predict/'
DATA_PREDICTION_JSON_PATH = '../predict_data_text/'



place_names = ['PhanDinhGiot-PhanThucDuyen','QuangTrung-LeVanTho','PVD-Phan van Tri',
                'PhanDangLuu-TrangLong','Nga4bon xa','VoVanKiet-HoNgocLam','DH-NongLam'             #DinhBoLinh-BachDang(5)old
                ,"NamKyKhoiNghia-TranQuocToan","TruongChinh-AuCo","TruongChinh-TanKyTanQuy",
                "LeVanSy-HoangVanThu","TruongSon-GaNoi","NgVanCu-TranHungDao",
                "NgHuuCanh-NgBinhKhiem","MaiChiTho-LuongDC","XaLoHN-TayHoa"]
places_rm = ['P+P','Q+L','PD+P','PL+NT','Nga4','DB+B','DH+N',"NK+T","TC+AC","TC+TK","LVS+HT","TS+QN","NVC+THD","NHC+NBK","MCT+LDC","XLHN+TH"]
class_names = ['Vang','Thua thot','Trung binh','Dong','Ket xe']
class_names_en = ['empty','sparse','medium','crowded','traffic jam']

image_width = 64
image_height = 128
num_images = 4036
# Parameters
learning_rate = 0.00001
num_steps = 2000
batch_size = 150    # numbers of training images for a step
display_step = 100

# Network Parameters
strLayer = (load_weight_path.split('#')[1]).split('-')
n_hidden_1 = 768 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 256 # 3nd layer number of neurons
num_input = 24576 # MNIST data input (img shape: 64*128)
num_classes = 5 # MNIST total classes (0-4)

#load_weight_path = re.sub(["n_hidden_1","n_hidden_2","n_hidden_3"],[n_hidden_1,n_hidden_2,n_hidden_3],load_weight_path)

# Define the neural network. To use eager API and tf.layers API together,
# we must instantiate a tfe.Network c2940lass as follow:
class MNISTModel(tf.keras.Model):
  def __init__(self):
    super(MNISTModel, self).__init__()
    self.dense1 = tf.keras.layers.Dense(units = n_hidden_1,activation = tf.nn.relu)
    self.dense2 = tf.keras.layers.Dense(units = n_hidden_2,activation = tf.nn.relu)
    self.dense3 = tf.keras.layers.Dense(units = n_hidden_3,activation = tf.nn.relu)
    self.denseout = tf.keras.layers.Dense(units = num_classes)

  def call(self, input):
    """Run the model."""
    result = self.dense1(input)
    result = self.dense2(result)
    result = self.dense3(result)
    result = self.denseout(result)  # reuse variables from dense2 layer
    return result

def modelLearn():
    model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
        keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
        keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
    return model





'''
class NeuralNet(tfe.Network):
    def __init__(self):
        # Define each layer
        super(NeuralNet, self).__init__()
        # Hidden fully connected layer with 256 neurons
        self.layer1 = self.track_layer(
            tf.layers.Dense(n_hidden_1, activation=tf.nn.relu))
        # Hidden fully connected layer with 256 neurons
        self.layer2 = self.track_layer(
            tf.layers.Dense(n_hidden_2, activation=tf.nn.relu))
        # Output fully connected layer with a neuron for each class
        self.out_layer = self.track_layer(tf.layers.Dense(num_classes))

    def call(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        return self.out_layer(x)
neural_net = NeuralNet()
'''




# Cross-Entropy loss function
def loss_fn(inference_fn, inputs, labels):
    # Using sparse_softmax cross entropy
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=inference_fn(inputs), labels=labels))

# Calculate accuracy
def accuracy_fn(inference_fn, inputs, labels):
    prediction = tf.nn.softmax(inference_fn(inputs))
    #print(prediction)
    correct_pred = tf.equal(tf.argmax(prediction, 1), labels)
    return tf.reduce_mean(tf.cast(correct_pred, tf.float32))

def get_prediction(inference_fn, inputs, labels):
    return tf.nn.softmax(inference_fn(inputs))

'''
def saveModel():
    export_dir = "../mod"
    builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
    with tf.Session(graph=tf.Graph()) as sess:
      builder.add_meta_graph_and_variables(sess,[tf.saved_model.tag_constants.TRAINING])
    with tf.Session(graph=tf.Graph()) as sess:
      builder.add_meta_graph(["bar-tag", "baz-tag"])
    builder.save()

def loadModel():
    export_dir = "../mod"
    with tf.Session(graph=tf.Graph()) as sess:
      tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.TRAINING], export_dir)
'''
#loadModel()

def get_traindata():
    train_images = np.loadtxt(TRAIN_DATA_PATH)
    #print(train_images.shape) 989 images (989, 64, 384)
    print(train_images.shape)
    num_ima = int(train_images.shape[0]/image_width)
    total_size = int(train_images.shape[1]*image_width) # 64*384 = 24576
    train_images = train_images.reshape((num_ima,total_size))
    #print(train_images.shape)
    train_labels = np.loadtxt(TRAIN_LABES_PATH)
    train_labels = train_labels.astype(int)
    return train_images,train_labels

def get_testdata():
    test_image = np.loadtxt(TEST_DATA_PATH)
    num_ima = int(test_image.shape[0]/image_width)
    total_size = int(test_image.shape[1]*image_width) # 64*384 = 24576
    testX = test_image.reshape((num_ima,total_size))
    test_labels = np.loadtxt(TEST_LABES_PAHT)
    testY = test_labels.astype(int)
    return testX, testY

def get_predictData():
    test_images = np.loadtxt(TEST_DATA_PATH)
    num_ima = int(test_images.shape[0]/image_width)
    test_images = test_images.reshape((num_ima,image_width,test_images.shape[1])).astype(int)
    test_labels = np.loadtxt(TEST_LABES_PAHT)
    test_labels = test_labels.astype(int)
    return test_images, test_labels

# Training
def training():
    rate = [0.00001]
    batch = [140,160,180]
    test_acuracy = 0;
    br = False
    for bat in batch:
        batch_size = bat
        for value_rate in rate:
            learning_rate = value_rate
            model = MNISTModel()

            train_images,train_labels = get_traindata()

            dataset = tf.data.Dataset.from_tensor_slices(
                (train_images,train_labels))
            dataset = dataset.repeat().batch(batch_size).prefetch(batch_size)
            dataset_iter = tfe.Iterator(dataset)
            # SGD Optimizer
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            # Compute gradients
            grad = tfe.implicit_gradients(loss_fn)
            average_loss = 0.
            average_acc = 0.
            for step in range(num_steps):

                # Iterate through the dataset
                d = dataset_iter.next()

                # Images
                x_batch = d[0]
                # Labels
                y_batch = tf.cast(d[1], dtype=tf.int64)

                # Compute the batch loss
                batch_loss = loss_fn(model, x_batch, y_batch)
                average_loss += batch_loss
                # Compute the batch accuracy
                batch_accuracy = accuracy_fn(model, x_batch, y_batch)
                average_acc += batch_accuracy

                if step == 0:
                    # Display the initial cost, before optimizing
                    print("Initial loss= {:.9f}".format(average_loss))

                # Update the variables following gradients info
                optimizer.apply_gradients(grad(model, x_batch, y_batch))

                # Display info
                if (step + 1) % display_step == 0 or step == 0:
                    if step > 0:
                        average_loss /= display_step
                        average_acc /= display_step
                    print("Step:", '%04d' % (step + 1), " loss=",
                          "{:.9f}".format(average_loss), " accuracy=",
                          "{:.4f}".format(average_acc))
                    if(average_acc>=0.90):
                        load_weight_path = TRAIN_WEIGHT_PATH+str(batch_size)+"+"+str(learning_rate)+"#"+str(n_hidden_1)+"-"+str(n_hidden_2)+"-"+str(n_hidden_3)+"#"+str(int(average_acc*100))+".h5"
                        model.save_weights(load_weight_path)
                        # model.summary()
                    test_acuracy = average_acc
                    average_loss = 0.
                    average_acc = 0.
            if(test_acuracy > 0.94):
                br = True
        if(br):
            break

    #model.save_weights(TRAIN_WEIGHT_PATH)
    testX,testY = get_testdata()
    test_acc = accuracy_fn(model, testX, testY)
    print("Testset Accuracy: {:.4f}".format(test_acc))
    #model.summary()

#training()

def main():
    training()

if __name__ == '__main__':
    main()
