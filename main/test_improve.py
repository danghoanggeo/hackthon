from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from os.path import getsize, isfile, isdir, join
from os import remove, rename, walk, stat
from PIL import Image
import math
from io import BytesIO
import time
import urllib
import requests
import json
import re

# Set Eager API
tf.enable_eager_execution()
tfe = tf.contrib.eager



load_weight_path = "../model/4646_weights170+1e-05#768-512-256#92.h5"
IMAGES_SAVE_PATH = '../predict/test_improve/'
IMAGES_PATH = '../predict/06-10-2018'
DATA_PREDICTION_JSON_PATH = '../predict_data_text/'


image_width = 64
image_height = 128
num_images = 4646
# Parameters
learning_rate = 0.00001
num_steps = 2000
batch_size = 150    # numbers of training images for a step
display_step = 100

# Network Parameters
strLayer = (load_weight_path.split('#')[1]).split('-')
n_hidden_1 = 768 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 256 # 3nd layer number of neurons
num_input = 24576 # MNIST data input (img shape: 64*128)
num_classes = 5 # MNIST total classes (0-4)

#load_weight_path = re.sub(["n_hidden_1","n_hidden_2","n_hidden_3"],[n_hidden_1,n_hidden_2,n_hidden_3],load_weight_path)

# Define the neural network. To use eager API and tf.layers API together,
# we must instantiate a tfe.Network c2940lass as follow:
class MNISTModel(tf.keras.Model):
  def __init__(self):
    super(MNISTModel, self).__init__()
    self.dense1 = tf.keras.layers.Dense(units = n_hidden_1,activation = tf.nn.relu)
    self.dense2 = tf.keras.layers.Dense(units = n_hidden_2,activation = tf.nn.relu)
    self.dense3 = tf.keras.layers.Dense(units = n_hidden_3,activation = tf.nn.relu)
    self.denseout = tf.keras.layers.Dense(units = num_classes)

  def call(self, input):
    """Run the model."""
    result = self.dense1(input)
    result = self.dense2(result)
    result = self.dense3(result)
    result = self.denseout(result)  # reuse variables from dense2 layer
    return result

def modelLearn():
    model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
        keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
        keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
    return model





'''
class NeuralNet(tfe.Network):
    def __init__(self):
        # Define each layer
        super(NeuralNet, self).__init__()
        # Hidden fully connected layer with 256 neurons
        self.layer1 = self.track_layer(
            tf.layers.Dense(n_hidden_1, activation=tf.nn.relu))
        # Hidden fully connected layer with 256 neurons
        self.layer2 = self.track_layer(
            tf.layers.Dense(n_hidden_2, activation=tf.nn.relu))
        # Output fully connected layer with a neuron for each class
        self.out_layer = self.track_layer(tf.layers.Dense(num_classes))

    def call(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        return self.out_layer(x)
neural_net = NeuralNet()
'''




# Cross-Entropy loss function
def loss_fn(inference_fn, inputs, labels):
    # Using sparse_softmax cross entropy
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=inference_fn(inputs), labels=labels))

# Calculate accuracy
def accuracy_fn(inference_fn, inputs, labels):
    prediction = tf.nn.softmax(inference_fn(inputs))
    #print(prediction)
    correct_pred = tf.equal(tf.argmax(prediction, 1), labels)
    return tf.reduce_mean(tf.cast(correct_pred, tf.float32))

def get_prediction(inference_fn, inputs, labels):
    return tf.nn.softmax(inference_fn(inputs))

'''
def saveModel():
    export_dir = "../mod"
    builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
    with tf.Session(graph=tf.Graph()) as sess:
      builder.add_meta_graph_and_variables(sess,[tf.saved_model.tag_constants.TRAINING])
    with tf.Session(graph=tf.Graph()) as sess:
      builder.add_meta_graph(["bar-tag", "baz-tag"])
    builder.save()

def loadModel():
    export_dir = "../mod"
    with tf.Session(graph=tf.Graph()) as sess:
      tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.TRAINING], export_dir)
'''
#


def resize_crop(image,size):
    img_format = image.format
    img = image.copy()
    old_size = img.size
    left = (old_size[0] - size[0]) / 2
    top = (old_size[1] - size[1]) / 2
    right = (old_size[0] - left)
    bottom = old_size[1] - top
    rect = [int(math.ceil(x)) for x in (left,top,right,bottom)]
    left, top, right, bottom = rect
    crop = image.crop((left,top,right,bottom))
    crop.format = img_format
    return crop


def resize_cover(image,size):
    img_format = image.format
    img = image.copy()
    img_size = img.size
    ratio = max(size[0] / img_size[0],size[1] / img_size[1])
    new_size = [int(math.ceil(img_size[0] * ratio)),int(math.ceil(img_size[1]*ratio))]
    img = img.resize((new_size[0],new_size[1]),Image.LANCZOS)
    img = resize_crop(img,size)
    img.format = img_format
    return img

def read_image_data(filename):
    try:
        img = Image.open(filename)
        img = resize_cover(img,[image_height,image_width])
        array = np.array(img).reshape(image_width,image_height*3)
        return array.reshape((1,-1))
    except Exception as e:
        return []

def showPrediction(place_index,array,prediction):
    try:
        int(np.argmax(prediction))
        fig = plt.figure()
        plt.title(place_names[place_index])
        plt.imshow(array.reshape(image_width,image_height,3))
        plt.colorbar()
        plt.grid(False)
        plt.xlabel("{} {:2.0f}%".format(class_names[int(np.argmax(prediction))],
                        100*np.max(prediction),
                        color = 'red'))
        #plt.show()
        fig.savefig(figureSave)
    except Exception as e:
        return 0

def savePredictData(fileName,data):
    try:
        path = DATA_PREDICTION_JSON_PATH + fileName+".txt"
        with open(path,'w') as fp:
            np.savetxt(path,data,fmt="%d")
        return 1
    except Exception as e:
        return 0

def printPrediction(place_index,prediction):
    print(place_names[place_index]+":------->"+"{} {:2.0f}%".format(class_names[int(np.argmax(prediction))],100*np.max(prediction)))

def showImagePredict(imagesArr,predictions):
    try:
        num_rows = 4
        num_cols = int(len(places_rm)/num_rows)
        num_images = len(imagesArr) # len(imagesArr)
        fig = plt.figure(figsize=(2*2*num_cols,2*num_rows))
        for i in range(num_images):
            plt.grid(False)
            plt.xticks([])
            plt.yticks([])
            plt.subplot(num_rows,num_cols,i+1)
            plt.title(place_names[i])
            if len(imagesArr[i])== 1 :
                plt.imshow(imagesArr[i].reshape(image_width,image_height,3),cmap=plt.cm.binary)
                plt.xlabel("{} {:2.0f}% ".format(class_names[int(np.argmax(predictions[i]))],100*np.max(predictions[i])))
            else:
                plt.xlabel("Could not load images")
        plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.5,wspace=0.35)
        plt.suptitle('Traffic condition at: {}'.format(time.strftime("%H:%M: %a, %d %b %Y ")), fontsize=16)
        plt.show()
        #fig.savefig(FIGURE_SAVE_PATH+time.strftime("%H:%M:%S%a")+".png")
        #plt.close('all')
    except Exception as e:
        print(e)
        return 0

def predicting():
    #load model weights
    model = modelLearn()
    model.load_weights(load_weight_path)
    #model.summary()
    model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])
    total = 0
    numDif = 0
    try:
        for root, dirs, files in walk(IMAGES_PATH):
            for file in files:
                fullpath = join(root, file)
                predict_image = read_image_data(fullpath)
                img = Image.open(fullpath)
                name = file.split('.')[0]
                currentLevel = int(name.split(':')[4])
                if(len(predict_image) > 0):
                    prediction = model.predict(predict_image)
                    new_level = int(np.argmax(prediction)) +1      # 0 ~ 4
                    if(new_level != currentLevel):
                        numDif += 1
                    newName = "%s-%d.jpg"%(name,new_level)
                    print("fixed: %d"%(numDif))
                    img.save(IMAGES_SAVE_PATH+newName)
                    total += 1
                else:
                    print("Null----")
    except Exception as e:
        print(e)
    print("Number of fixd images: %d, %.2f percent"%(numDif,(numDif/total)*100))
def main():
    predicting()

if __name__ == '__main__':
    main()
