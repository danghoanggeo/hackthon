from __future__ import print_function
from tkinter import *
from PIL import Image, ImageFile, ImageTk
from sys import exit, stderr
from os.path import abspath
from os import listdir
from stat import S_IWRITE
from shutil import move
from argparse import ArgumentParser
from abc import ABCMeta, abstractmethod
from resizeimage import resizeimage
import numpy as np
import re
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import math
from io import BytesIO
import time
import json

# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class_names = ['Vang','Thua thot','Trung binh','Dong','Ket xe']
class_names_en = ['empty','sparse','medium','crowded','traffic jam']

image_width = 64
image_height = 128
num_images = 4036
# Parameters
learning_rate = 0.00001
num_steps = 2000
batch_size = 150    # numbers of training images for a step
display_step = 100

# Network Parameters
n_hidden_1 = 768 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 256 # 3nd layer number of neurons
num_input = 24576 # MNIST data input (img shape: 64*128)
num_classes = 5 # MNIST total classes (0-4)
load_weight_path = "../model/3841_weigth_group2_day160+1e-05#768-512-256#90.h5"

class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):

        # parameters that you want to send through the Frame class.
        Frame.__init__(self, master)

        #reference to the master widget, which is the tk window
        self.master = master
        self.index = 0
        self.DIRECTORY = "../images/group2_day_predict"
        self.IMAGE_PATH_FIXED = "../images/"
        self.IMAGE_PATH_FIXED_GROUP = "../images/"
        self.files_path = []
        self.currentLevel = 0
        #with that, we want to then run init_window, which doesn't yet exist
        self.init_window()
        self.init_data()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget
        self.master.title("GUI")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


        # create the file object)
        edit = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        edit.add_command(label="Show Img", command=self.showImg)
        edit.add_command(label="Show Text", command=self.showText)

        #added "file" to our menu
        menu.add_cascade(label="Edit", menu=edit)
        self.createWidgets()


    def init_data(self):
        self.files_path = [files for files in listdir(self.DIRECTORY)]
        #print(self.files_path[self.index])
        self.showImg(self.files_path[self.index])

    def show_msg(self,msg):
        if tkinter.messagebox.askokcancel("Inform", msg):
            print ("hi there, everyone!")

    def next(self):
        self.index += 1
        self.showImg(self.files_path[self.index])

    def getGroup(self,index):
        if(index == 3 or index == 4 or index == 10 or index == 12 or index == 13 or index ==14 or index == 15):
            return 1
        else:
            return 2

    def modelLearn(self):
        model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
            keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
            keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
            keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
            keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
        return model

    def resize_crop(self,image,size):
        img_format = image.format
        img = image.copy()
        old_size = img.size
        left = (old_size[0] - size[0]) / 2
        top = (old_size[1] - size[1]) / 2
        right = (old_size[0] - left)
        bottom = old_size[1] - top
        rect = [int(math.ceil(x)) for x in (left,top,right,bottom)]
        left, top, right, bottom = rect
        crop = image.crop((left,top,right,bottom))
        crop.format = img_format
        return crop


    def resize_cover(self,image,size):
        img_format = image.format
        img = image.copy()
        img_size = img.size
        ratio = max(size[0] / img_size[0],size[1] / img_size[1])
        new_size = [int(math.ceil(img_size[0] * ratio)),int(math.ceil(img_size[1]*ratio))]
        img = img.resize((new_size[0],new_size[1]),Image.LANCZOS)
        img = self.resize_crop(img,size)
        img.format = img_format
        return img

    def read_image_data(self,url):
        try:
            img = Image.open(url)
            img = self.resize_cover(img,[image_height,image_width])
            array = np.array(img).reshape(image_width,image_height*3)
            return array.reshape((1,-1))
        except Exception as e:
            print(e)
            return []

    def predicting(self,url):
        #Connect to database
        model = self.modelLearn()
        model.load_weights(load_weight_path)
        #model.summary()
        model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])
        predict_image = self.read_image_data(url)
        if(len(predict_image) > 0):
            prediction = model.predict(predict_image)
            return int(np.argmax(prediction))+1
        else:
            return -1

    def save_img(self):
        imageName = self.files_path[self.index]
        name = imageName.split('.')[0]
        names = name.split(":")
        self.currentLevel = names[4]
        name = "%s:%s:%s:%s-lb.jpg"%(names[0],names[1],names[2],names[3])
        #name = "%s-lb.jpg"%(names[0])
        night = False
        if((int(names[2])>=17 and int(names[3])>30) or int(names[2])>=18):
            night = True
        #name = "%s-lb.jpg"%(names[0])
        img = Image.open(self.DIRECTORY+"/"+imageName)
        imgArr = np.array(img)
        intNum = self.label_in.get()
        if(intNum == ""):
            intNum = self.currentLevel
        name = re.sub("lb",intNum,name)
        group_path = self.IMAGE_PATH_FIXED_GROUP
        #group = self.getGroup(int(names[0]))
        group = int(self.group.get())
        if(group == 1):
            if(night):
                group_path = group_path+"group1_night/"
            else:
                group_path = group_path+"group1_day/"
        if(group == 2):
            if(night):
                group_path = group_path+"group2_night/"
            else:
                group_path = group_path+"group2_day/"

        #img.save(self.IMAGE_PATH_FIXED+name)
        img.save(group_path+name)
        self.index += 1
        if(self.index < len(self.files_path)):
            self.label_in.delete(0,last=None)
            print(name)
            self.showImg(self.files_path[self.index])
        else:
            print("Finished!")

    def createWidgets(self):
        self.QUIT = Button(self, height=2, width=5)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit
        self.QUIT.place(x=10,y=10)

        self.hi_there = Button(self, height=2, width=5)
        self.hi_there["text"] = "Next",
        self.hi_there["command"] = self.next
        self.hi_there.place(x=80,y=10)

        self.print_in = Button(self, height=2, width=5)
        self.print_in["text"] = "Ok",
        #self.print_in["command"] = self.save_img
        self.print_in.place(x=160,y=10)

        self.L1 = Label(self, text="level: ")
        self.L1.place(x=240,y=10)
        self.task_finished = Label(self, text="Fixed img:%d / %d "%(self.index,len(self.files_path)))
        self.task_finished.place(x=480,y=10)

        self.label_in = Entry(self)
        self.label_in.place(x=240,y=30)
        self.L2 = Label(self, text="group: ")
        self.L2.place(x=240,y=60)
        self.group = Entry(self)
        self.group.place(x=240,y=80)

    def updateInfo(self,imageName,predict):
        name = imageName.split('.')[0]
        names = name.split(":")
        label = names[4]
        self.L1["text"] = "label: %s, predict: %d"%(label,predict)
        #self.L2["text"] = "group: %d"%self.getGroup(int(names[0]))
        self.task_finished["text"] = "Fixed img:%d / %d "%(self.index,len(self.files_path))

    def showImg(self,imageName):
        predict = self.predicting(self.DIRECTORY+"/"+imageName)
        self.updateInfo(imageName,predict)
        load = Image.open(self.DIRECTORY+"/"+imageName)
        render = ImageTk.PhotoImage(load)
        # labels can be text or images
        if(self.index == 0):
            self.img = Label(self,image = render)
            self.img.image = render
        else:
            self.img.config(image='')
            self.img = Label(self,image = render)
            self.img.image = render
        self.img.place(x=20, y=120)


    def showText(self):
        text = Label(self, text="Hey there good lookin!")
        text.pack()


    def client_exit(self):
        exit()


# root window created. Here, that would be the only window, but
# you can later have windows within windows.
root = Tk()

root.geometry("1200x720")

#creation of an instance
app = Window(root)


#mainloop
root.mainloop()
