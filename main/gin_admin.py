# Simple enough, just import everything from tkinter.
from tkinter import *
from PIL import Image, ImageFile, ImageTk
from sys import exit, stderr
from os.path import abspath
from os import listdir
from stat import S_IWRITE
from shutil import move
from argparse import ArgumentParser
from abc import ABCMeta, abstractmethod
from resizeimage import resizeimage
import numpy as np
import re
# firebase admin
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import Geohash as gh

FIREBASE_CERTIFICATE = '../firebase/gshare-1744f-firebase-adminsdk-viomo-693dc8ccf5.json'
FIREBASE_APP_URL = 'https://gshare-1744f.firebaseio.com'
FIREBAE_REF_CAMERA_VAL = 'dythaydoinamic/traffic_cam'
FIREBAE_REF_CAMERA = 'stakhongdoitic/traffic_camera'
# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):

        # parameters that you want to send through the Frame class.
        Frame.__init__(self, master)

        #reference to the master widget, which is the tk window
        self.master = master
        self.index = 0
        self.DIRECTORY = "../predict/not_fixed"
        self.IMAGE_PATH_FIXED = "../predict/fixed/"
        self.files_path = []
        self.currentLevel = 0
        #with that, we want to then run init_window, which doesn't yet exist
        cred = credentials.Certificate(FIREBASE_CERTIFICATE)
        default_app = firebase_admin.initialize_app(cred,{'databaseURL': FIREBASE_APP_URL})
        #load model weights
        self.init_window()
        #self.init_data()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget
        self.master.title("GUI")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


        # create the file object)
        edit = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        edit.add_command(label="Save", command=self.save_camera)
        edit.add_command(label="Show Text", command=self.showText)

        #added "file" to our menu
        menu.add_cascade(label="Edit", menu=edit)
        self.createWidgets()


    #def init_data(self):
        #self.files_path = [files for files in listdir(self.DIRECTORY)]
        #print(self.files_path[self.index])
        #self.showImg(self.files_path[self.index])

    def show_msg(self,msg):
        if tkinter.messagebox.askokcancel("Inform", msg):
            print ("hi there, everyone!")

    def next(self):
        print ("hi there, everyone!")

    def save_camera(self):
        #Connect to database
        try:
            traffic_came_ref = db.reference(FIREBAE_REF_CAMERA_VAL)
            came_ref = db.reference(FIREBAE_REF_CAMERA)
            name = self.camera_name.get()
            if(name == ""):
                self.noti['text'] = "Name cannot be blank!"
            else:
                group = self.camer_group.get()
                if(group == ""):
                    group = 1
                latLonstr = self.camera_latlon.get()
                if(latLonstr == ""):
                    self.noti['text'] = "Lat long cannot be blank!"
                else:
                    lat,lon = self.getLatLon(latLonstr)
                    geocode = gh.encode(lat,lon)
                    camera_id = geocode+"-"+group+"-"+str(self.index)
                    link = self.camer_link.get()
                    if(link == "" ):
                        self.noti['text'] = "Link cannot be blank!"
                    else:
                        camera_detail = {
                                'g': geocode,
                                'l': {0:lat,1:lon},
                                'name':name,
                                'link':link}
                        camera_val = {
                                'g': geocode,
                                'l': {0:lat,1:lon},
                                'val':0}
                        traffic_came_ref.child(camera_id).set(camera_val)
                        came_ref.child(camera_id).set(camera_detail)
                        self.index += 1
                        self.clear_camera()
                        print("save %s"%(name))
            self.noti['text'] = ""
        except Exception as e:
            print(e)


    def getLatLon(self,str):
        latlon = str.split(',')
        lat = float(latlon[0])
        lon = float(latlon[1])
        return lat,lon


    def showText(self):
        text = Label(self, text="Hey there good lookin!")
        text.pack()
    def clear_camera(self):
        self.camera_name.delete(0,'end')
        self.camera_latlon.delete(0,'end')
        self.camer_group.delete(0,'end')
        self.camer_link.delete(0,'end')

    def createWidgets(self):
        self.QUIT = Button(self, height=2, width=5)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit
        self.QUIT.place(x=10,y=10)

        self.hi_there = Button(self, height=2, width=5)
        self.hi_there["text"] = "Next",
        self.hi_there["command"] = self.next
        self.hi_there.place(x=80,y=10)
        self.noti = Label(self, text="")
        self.noti.place(x=320,y=10)


        self.saveCamera = Button(self, height=2, width=5)
        self.saveCamera["text"] = "Ok",
        self.saveCamera["command"] = self.save_camera
        self.saveCamera.place(x=120,y=280)
        self.cancel = Button(self, height=2, width=5)
        self.cancel["text"] = "Cancel",
        self.cancel["command"] = self.clear_camera
        self.cancel.place(x=30,y=280)

        self.L1 = Label(self, text="Name: ")
        self.L1.place(x=10,y=60)
        self.camera_name = Entry(self)
        self.camera_name.place(x=80,y=60)
        self.L2= Label(self, text="Lat ,Long: ")
        self.L2.place(x=10,y=120)
        self.camera_latlon = Entry(self)
        self.camera_latlon.place(x=80,y=120)


        self.L3= Label(self, text="group: ")
        self.L3.place(x=10,y=180)
        self.camer_group = Entry(self)
        self.camer_group.place(x=80,y=180)

        self.L4= Label(self, text="link: ")
        self.L4.place(x=10,y=240)
        self.camer_link = Entry(self)
        self.camer_link.place(x=80,y=240)

    def client_exit(self):
        exit()


# root window created. Here, that would be the only window, but
# you can later have windows within windows.
root = Tk()

root.geometry("1200x720")

#creation of an instance
app = Window(root)


#mainloop
root.mainloop()
