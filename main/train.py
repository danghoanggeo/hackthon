
#Tensorflow and tf.keras

import tensorflow as tf
from tensorflow import keras

#Helper libraries

import numpy as np
import matplotlib.pyplot as plt

#print(tf.__version__)

new_model =  keras.models.load_model('traffic_model.h5')
new_model.summary()

train_images = np.loadtxt('train_data.txt')
train_images = train_images.reshape((870,64,384))
train_labels = np.loadtxt('train_labes.txt')
train_labels = train_labels.astype(int)

test_images = np.loadtxt('test_data.txt')
test_images = test_images.reshape((228,64,384))
test_labels = np.loadtxt('test_labes.txt')
test_labels = test_labels.astype(int)

class_names = ['Vang','Thua thot','Trung binh','Dong','Ket xe']

# Preprocess the data
#print(train_images[0])
#plt.figure()
#plt.imshow(train_images[0])
#plt.colorbar()
#plt.grid(False)
#plt.show()
# Important that the training set and the testing set are preprocessed in the same way

train_images = train_images / 255.0
test_images = test_images / 255.0

# Display the first 25 images from the training set and disply the class name below each image.

plt.figure(figsize = (10,10))
for i in range(25):
	plt.subplot(5,5,i+1)
	plt.xticks([])
	plt.yticks([])
	plt.grid(False)
	plt.imshow(train_images[i].reshape(64,128,3),cmap = plt.cm.binary)
	plt.xlabel(class_names[int(train_labels[i])])
plt.show()


	#Build The model
if(False):
	print('Build new model')
	model = keras.Sequential([keras.layers.Flatten(input_shape=(64,384)),
	    keras.layers.Dense(3200,activation = tf.nn.relu),
	    keras.layers.Dense(5,activation=tf.nn.softmax)])


	# The first layer in this network, tf.layers.flatten transforms the format of the images form a 2d-array (28x28) to a
	# 1d-array of 28*28 = 784 pixels.

	# after the pixels are flattened, the network consists of a sequence of two tf.keras.layers.Dense layers.
	#the first Dense layer has 128 nodes(or neurons). The second (and last) layer is a 10-node softmax layer -
	# this returns an array of 10 proability scores that sum to 1.


	#Complie the model
	model.compile(optimizer=tf.train.AdamOptimizer(),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

	#Train the model
	model.fit(train_images,train_labels,epochs = 5)
	model.save('traffic_model.h5')
	new_model = model

#Complie the model
#new_model.compile(optimizer=tf.train.AdamOptimizer(),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

#Evaluate accuracy
test_loss, test_acc  = new_model.evaluate(test_images,test_labels)

print('Test accuracy:',test_acc)


predictions = new_model.predict(test_images)
print(predictions)

def plot_image(i,predictions_array,true_label,img):
	predictions_array,true_label,img = predictions_array[i],true_label[i],img[i]
	plt.grid(False)
	plt.xticks([])
	plt.yticks([])

	plt.imshow(img.reshape(64,128,3),cmap=plt.cm.binary)

	predicted_label = np.argmax(predictions_array)
	if predicted_label  == true_label:
		color = 'blue'
	else:
		color = 'red'
	plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
					100*np.max(predictions_array),
					class_names[int(true_label)]),
					color = color)
def plot_value_array(i,predictions_array,true_label):
	predictions_array,true_label = predictions_array[i],true_label[i]
	plt.grid(False)
	plt.xticks([])
	plt.yticks([])
	thisplot = plt.bar(range(5),predictions_array, color = "#777777")
	plt.ylim = ([0,1])
	predicted_label = np.argmax(predictions_array)

	thisplot[predicted_label].set_color('red')
	thisplot[true_label].set_color('blue')


#Plot the first X test images, their predicted label, and  the true label
# Color correct predictions in blue, incorrect predictions in red

num_rows = 5
num_cols = 3
num_images = num_rows*num_cols
plt.figure(figsize=(2*2*num_cols,2*num_rows))

for i in range(num_images):
	plt.subplot(num_rows,2*num_cols,2*i+1)
	plot_image(i,predictions,test_labels,test_images)
	plt.subplot(num_rows,2*num_cols,2*i+2)
	plot_value_array(i,predictions,test_labels)
plt.show()
