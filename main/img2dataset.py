from PIL import Image, ImageFile
from sys import exit, stderr
from os.path import getsize, isfile, isdir, join
from os import remove, rename, walk, stat
from stat import S_IWRITE
from shutil import move
from argparse import ArgumentParser
from abc import ABCMeta, abstractmethod
from resizeimage import resizeimage
import numpy as np
import re

DATA_PATH = '../train_data'
DATASET_PATH_DATA = "../dataset/num_train_data.txt"
DATASET_PATH_LABELS = "../dataset/num_labes_data.txt"
'''
DATA_PATH = '../test_data'
DATASET_PATH_DATA = "../dataset/num_test_data.txt"
DATASET_PATH_LABELS = "../dataset/num_test_labes.txt"
'''
class ProcessBase:
    """Abstract base class for file processors."""
    __metaclass__ = ABCMeta



    def __init__(self):
        self.extensions = []
        self.backupextension = 'compressimages-backup'

    @abstractmethod
    def processfile(self, filename):
        """Abstract method which carries out the process on the specified file.
        Returns True if successful, False otherwise."""
        pass

    def processdir(self, path):
        """Recursively processes files in the specified directory matching
        the self.extensions list (case-insensitively)."""

        filecount = 0 # Number of files successfully updated
        fileN = []
        imgArrLs = []
        for root, dirs, files in walk(path):
            for file in files:
                # Check file extensions against allowed list
                lowercasefile = file.lower()
                matches = False
                for ext in self.extensions:
                    if lowercasefile.endswith('.' + ext):
                        matches = True
                        break
                if matches:
                    # File has eligible extension, so process
                    fullpath = join(root, file)
                    imgArr = self.processfile(fullpath)
                    if len(imgArr) > 0 :
                        fileName = fullpath.split('/')
                        imageName = fileName[len(fileName)-1]
                        label = imageName.split('.')[0]
                        #print(label.split('-')[1])
                        fileN.append(int(label.split('-')[1])-1)
                        imgArrLs.append(imgArr)
                        filecount = filecount + 1
        labelsName = np.asarray(fileN)
        imgArrNp = np.asarray(imgArrLs)
        print(imgArrNp.shape)
        np.savetxt(re.sub("num",str(filecount),DATASET_PATH_LABELS),labelsName,fmt="%d",delimiter=",")
        with open(re.sub("num",str(filecount),DATASET_PATH_DATA), 'w') as outfile:
            for slice_2d in imgArrNp:
                np.savetxt(outfile, slice_2d,fmt="%d")
                #np.savetxt("train_datas",imgArrNp)
        return filecount

class CompressImage(ProcessBase):
    """Processor which attempts to reduce image file size."""
    def __init__(self):
        ProcessBase.__init__(self)
        self.extensions = ['jpg', 'jpeg', 'png']

    def processfile(self, filename):

        arr2D = []

        try:
            # Open the image

            with open(filename, 'rb') as file:
                img = Image.open(file)
                img = resizeimage.resize_cover(img,[128,64])
                array = np.array(img)
                for arr in array:
                    arr = arr.reshape(-1)
                    arr2D.append(arr)

                #imgArrNp = np.asarray(arr2D)
                #print(imgArrNp.shape)
                #img.save(filename,img.format)
            # Successful resize
            #ok = True
        except Exception as e:
            stderr.write('Failure whilst processing "' + filename + '": ' + str(e) + '\n')
        return arr2D

if __name__ == "__main__":


     # Construct processor requested mode
    processor = CompressImage()
    if isfile(DATA_PATH):
        processor.processfile(DATA_PATH)
        print ('\nSuccessfully updated file')
    elif isdir(DATA_PATH):
        filecount = processor.processdir(DATA_PATH)
        print ('\nSuccessfully updated file count: ' + str(filecount))
    else:
        stderr.write('Invalid path "' + filename + '"\n')
        exit(1)
