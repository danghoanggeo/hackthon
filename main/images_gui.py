# Simple enough, just import everything from tkinter.
from tkinter import *
from PIL import Image, ImageFile, ImageTk
from sys import exit, stderr
from os.path import abspath
from os import listdir
from stat import S_IWRITE
from shutil import move
from argparse import ArgumentParser
from abc import ABCMeta, abstractmethod
from resizeimage import resizeimage
import numpy as np
import re


# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):

        # parameters that you want to send through the Frame class.
        Frame.__init__(self, master)

        #reference to the master widget, which is the tk window
        self.master = master
        self.index = 0
        self.DIRECTORY = "../images/group1_day_predict"
        self.IMAGE_PATH_FIXED = "../images/group1_day_predict_fixed"
        self.IMAGE_PATH_FIXED_GROUP = "../images/"
        self.files_path = []
        self.currentLevel = 0
        #with that, we want to then run init_window, which doesn't yet exist
        self.init_window()
        self.init_data()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget
        self.master.title("GUI")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


        # create the file object)
        edit = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        edit.add_command(label="Show Img", command=self.showImg)
        edit.add_command(label="Show Text", command=self.showText)

        #added "file" to our menu
        menu.add_cascade(label="Edit", menu=edit)
        self.createWidgets()


    def init_data(self):
        self.files_path = [files for files in listdir(self.DIRECTORY)]
        #print(self.files_path[self.index])
        self.showImg(self.files_path[self.index])

    def show_msg(self,msg):
        if tkinter.messagebox.askokcancel("Inform", msg):
            print ("hi there, everyone!")

    def next(self):
        self.index += 1
        self.showImg(self.files_path[self.index])

    def getGroup(self,index):
        if(index == 3 or index == 4 or index == 10 or index == 12 or index == 13 or index ==14 or index == 15):
            return 1
        else:
            return 2

    def save_img(self):
        imageName = self.files_path[self.index]
        name = imageName.split('.')[0]
        names = name.split(":")
        self.currentLevel = names[4]
        name = "%s:%s:%s:%s-lb.jpg"%(names[0],names[1],names[2],names[3])
        #name = "%s-lb.jpg"%(names[0])
        night = False
        if((int(names[2])>=17 and int(names[3])>30) or int(names[2])>=18):
            night = True
        #name = "%s-lb.jpg"%(names[0])
        img = Image.open(self.DIRECTORY+"/"+imageName)
        imgArr = np.array(img)
        intNum = self.label_in.get()
        if(intNum == ""):
            intNum = self.currentLevel
        name = re.sub("lb",intNum,name)
        group_path = self.IMAGE_PATH_FIXED_GROUP
        #group = self.getGroup(int(names[0]))
        group = int(self.group.get())
        if(group == 1):
            if(night):
                group_path = group_path+"group1_night/"
            else:
                group_path = group_path+"group1_day/"
        if(group == 2):
            if(night):
                group_path = group_path+"group2_night/"
            else:
                group_path = group_path+"group2_day/"

        #img.save(self.IMAGE_PATH_FIXED+name)
        img.save(group_path+name)
        self.index += 1
        if(self.index < len(self.files_path)):
            self.label_in.delete(0,last=None)
            print(name)
            self.showImg(self.files_path[self.index])
        else:
            print("Finished!")

    def createWidgets(self):
        self.QUIT = Button(self, height=2, width=5)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit
        self.QUIT.place(x=10,y=10)

        self.hi_there = Button(self, height=2, width=5)
        self.hi_there["text"] = "Next",
        self.hi_there["command"] = self.next
        self.hi_there.place(x=80,y=10)

        self.print_in = Button(self, height=2, width=5)
        self.print_in["text"] = "Ok",
        self.print_in["command"] = self.save_img
        self.print_in.place(x=160,y=10)

        self.L1 = Label(self, text="level: ")
        self.L1.place(x=240,y=10)
        self.task_finished = Label(self, text="Fixed img:%d / %d "%(self.index,len(self.files_path)))
        self.task_finished.place(x=320,y=10)

        self.label_in = Entry(self)
        self.label_in.place(x=240,y=30)
        self.L2 = Label(self, text="group: ")
        self.L2.place(x=240,y=60)
        self.group = Entry(self)
        self.group.place(x=240,y=80)

    def updateInfo(self,imageName):
        name = imageName.split('.')[0]
        names = name.split(":")
        label = names[4]
        self.L1["text"] = "level: %s"%(label)
        #self.L2["text"] = "group: %d"%self.getGroup(int(names[0]))
        self.task_finished["text"] = "Fixed img:%d / %d "%(self.index,len(self.files_path))

    def showImg(self,imageName):
        self.updateInfo(imageName)
        load = Image.open(self.DIRECTORY+"/"+imageName)
        render = ImageTk.PhotoImage(load)
        # labels can be text or images
        if(self.index == 0):
            self.img = Label(self,image = render)
            self.img.image = render
        else:
            self.img.config(image='')
            self.img = Label(self,image = render)
            self.img.image = render
        self.img.place(x=20, y=120)


    def showText(self):
        text = Label(self, text="Hey there good lookin!")
        text.pack()


    def client_exit(self):
        exit()


# root window created. Here, that would be the only window, but
# you can later have windows within windows.
root = Tk()

root.geometry("1200x720")

#creation of an instance
app = Window(root)


#mainloop
root.mainloop()
