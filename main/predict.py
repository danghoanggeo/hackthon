from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from PIL import Image
import math
from io import BytesIO
import time
import urllib
import requests
import json
import re

# Set Eager API
tf.enable_eager_execution()
tfe = tf.contrib.eager

image_width = 1
image_height = 4
num_images = 2940
# Parameters
learning_rate = 0.001
num_steps = 2000
batch_size = 140    # numbers of training images for a step
display_step = 50

# Network Parameters
n_hidden_1 = 768 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 256 # 3nd layer number of neurons
num_input = 4 # MNIST data input (place,day,hour,minute)
num_classes = 5# MNIST total classes (0-4)

PREDICT_FILE_PATH_TRAIN = '../temp/correct_predict.txt'
PREDICT_FILE_PATH_TEST = '../temp/test_predict.txt'
TRAIN_PREDICT_WEIGHT_PATH = "../predict/predict_weight"
load_weight_path = "../predict/predict_weight200+0.0001#768-512-256#52.h5"

place_names = ['PhanDinhGiot-PhanThucDuyen','QuangTrung-LeVanTho','PVD-Phan van Tri',
                'PhanDangLuu-TrangLong','Nga4bon xa','DinhBoLinh-BachDang','DH-NongLam'
                ,"NamKyKhoiNghia-TranQuocToan","TruongChinh-AuCo","TruongChinh-TanKyTanQuy",
                "LeVanSy-HoangVanThu","TruongSon-GaNoi","NgVanCu-TranHungDao",
                "NgHuuCanh-NgBinhKhiem","MaiChiTho-LuongDC","XaLoHN-TayHoa"]

# Define the neural network. To use eager API and tf.layers API together,
# we must instantiate a tfe.Network class as follow:
class MNISTModel(tf.keras.Model):
  def __init__(self):
    super(MNISTModel, self).__init__()
    self.dense1 = tf.keras.layers.Dense(units = n_hidden_1,activation = tf.nn.relu)
    self.dense2 = tf.keras.layers.Dense(units = n_hidden_2,activation = tf.nn.relu)
    self.dense3 = tf.keras.layers.Dense(units = n_hidden_3,activation = tf.nn.relu)
    self.denseout = tf.keras.layers.Dense(units = num_classes)

  def call(self, input):
    """Run the model."""
    result = self.dense1(input)
    result = self.dense2(result)
    result = self.dense3(result)
    result = self.denseout(result)  # reuse variables from dense2 layer
    return result

def modelLearn():
    model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
        keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
        keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
    return model

def get_traindata():
    data = np.loadtxt(PREDICT_FILE_PATH_TRAIN)
    train_labels = data[:,4]
    train_data = data[:,0:4]
    print(len(train_labels))
    return train_data,train_labels

def get_testdata():
    data = np.loadtxt(PREDICT_FILE_PATH_TEST)
    test_labels = data[:,4]
    test_data = data[:,0:4]
    print(len(test_labels))
    return test_data,test_labels


# Cross-Entropy loss function
def loss_fn(inference_fn, inputs, labels):
    # Using sparse_softmax cross entropy
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=inference_fn(inputs), labels=labels))

# Calculate accuracy
def accuracy_fn(inference_fn, inputs, labels):
    prediction = tf.nn.softmax(inference_fn(inputs))
    #print(prediction)
    correct_pred = tf.equal(tf.argmax(prediction, 1), labels)
    return tf.reduce_mean(tf.cast(correct_pred, tf.float32))

def get_prediction(inference_fn, inputs, labels):
    return tf.nn.softmax(inference_fn(inputs))

def training():
    rate = [0.0001]
    batch = [180,200]
    test_acuracy = 0;
    br = False
    for bat in batch:
        batch_size = bat
        for value_rate in rate:
            learning_rate = value_rate
            model = MNISTModel()

            train_images,train_labels = get_traindata()

            dataset = tf.data.Dataset.from_tensor_slices(
                (train_images,train_labels))
            dataset = dataset.repeat().batch(batch_size).prefetch(batch_size)
            dataset_iter = tfe.Iterator(dataset)
            # SGD Optimizer
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            # Compute gradients
            grad = tfe.implicit_gradients(loss_fn)
            average_loss = 0.
            average_acc = 0.
            for step in range(num_steps):

                # Iterate through the dataset
                d = dataset_iter.next()

                # Images
                x_batch = d[0]
                # Labels
                y_batch = tf.cast(d[1], dtype=tf.int64)

                # Compute the batch loss
                batch_loss = loss_fn(model, x_batch, y_batch)
                average_loss += batch_loss
                # Compute the batch accuracy
                batch_accuracy = accuracy_fn(model, x_batch, y_batch)
                average_acc += batch_accuracy

                if step == 0:
                    # Display the initial cost, before optimizing
                    print("Initial loss= {:.9f}".format(average_loss))

                # Update the variables following gradients info
                optimizer.apply_gradients(grad(model, x_batch, y_batch))

                # Display info
                if (step + 1) % display_step == 0 or step == 0:
                    if step > 0:
                        average_loss /= display_step
                        average_acc /= display_step
                    print("Step:", '%04d' % (step + 1), " loss=",
                          "{:.9f}".format(average_loss), " accuracy=",
                          "{:.4f}".format(average_acc))
                    if(average_acc>=0.5):
                        load_weight_path = TRAIN_PREDICT_WEIGHT_PATH+str(batch_size)+"+"+str(learning_rate)+"#"+str(n_hidden_1)+"-"+str(n_hidden_2)+"-"+str(n_hidden_3)+"#"+str(int(average_acc*100))+".h5"
                        model.save_weights(load_weight_path)
                        model.summary()
                    test_acuracy = average_acc
                    average_loss = 0.
                    average_acc = 0.
            #model.save_weights(TRAIN_WEIGHT_PATH)
            load_weight_path = TRAIN_PREDICT_WEIGHT_PATH+str(batch_size)+"+"+str(learning_rate)+"#"+str(n_hidden_1)+"-"+str(n_hidden_2)+"-"+str(n_hidden_3)+"#"+str(int(average_acc*100))+".h5"
            model.save_weights(load_weight_path)
            model.summary()
            testX,testY = get_testdata()
            test_acc = accuracy_fn(model, testX, testY)
            print("Testset Accuracy: {:.4f}".format(test_acc))
            if(test_acuracy > 0.95):
                br = True
        if(br):
            break

    #model.save_weights(TRAIN_WEIGHT_PATH)
    '''testX,testY = get_testdata()
    test_acc = accuracy_fn(model, testX, testY)
    print("Testset Accuracy: {:.4f}".format(test_acc))'''
    #model.summary()
def predict():
    #load model weights

    model = modelLearn()
    model.load_weights(load_weight_path)
    model.summary()
    model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

    #load data to predict
    arr = input("Enter place-day-hour-minute] to get predict: ")
    arr =arr.split('-')
    predict_data = [int(arr[0]),int(arr[1]),int(arr[2]),int(arr[3])]
    predict_data = np.asarray(predict_data)
    predict_data = predict_data.reshape((1,-1))
    print(predict_data.shape)
    predictions = model.predict(predict_data)

    print(predictions)
def main():
    #training()
    predict()
    #predicting()

if __name__ == '__main__':
    main()
