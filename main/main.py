from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from PIL import Image
import math
from io import BytesIO
import time
import urllib
import requests
import json
import re
# firebase admin
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


# Set Eager API
tf.enable_eager_execution()
tfe = tf.contrib.eager

FIREBASE_CERTIFICATE = '../firebase/gshare-1744f-firebase-adminsdk-viomo-693dc8ccf5.json'
FIREBASE_APP_URL = 'https://gshare-1744f.firebaseio.com'
FIREBAE_REF_VALUE = 'dythaydoinamic/traffic_cam'

TRAIN_WEIGHT_PATH = "../model/4646_weights"
load_weight_g2 = "../model/3841_weigth_group2_day160+1e-05#768-512-256#90.h5"
load_weight_g1 = "../model/1712_weigth_group1_day160+1e-05#768-512-256#95.h5"
TRAIN_DATA_PATH = '../dataset/4646_train_data.txt'  #../bk_00/bk_v1/image_processing/train_data.txt
TRAIN_LABES_PATH = '../dataset/4646_train_labes.txt'    #../bk_00/bk_v1/image_processing/
TEST_DATA_PATH = '../dataset/597_test_data.txt'
TEST_LABES_PAHT = '../dataset/597_test_labes.txt'
FIGURE_SAVE_PATH = '../figureSave/'
PREFIX_SAVE_IMAGE_PATH = '../images/'
DATA_PREDICTION_JSON_PATH = '../predict_data_text/'
IMAGES_JON_PATH = '../camera_info.json'


URL = ["http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=56df85bdc062921100c143e8","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=5a824dc05058170011f6eab2",
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=58af9670bd82540010390c34","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=587ee7d7b807da0011e33d55",
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=5a824b6c5058170011f6eaab","http://giaothong.hochiminhcity.gov.vn:8007/Render/CameraHandler.ashx?id=56de42f611f398ec0c48129a",#http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=5a8253bc5058170011f6eac1
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=58746122b807da0011e33cc5","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=58e49ff8d9d6200011e0b9d4",
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=56df807bc062921100c143da","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=586e25e1f9fab7001111b0ae",
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=587c8661b807da0011e33d43","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=56f8d743025e9511002786c5",
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=5b0b7bbe0e517b00119fd806","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=58af99abbd82540010390c39",
        "http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=56de42f611f398ec0c481280","http://giaothong.hochiminhcity.gov.vn/render/ImageHandler.ashx?id=56df8274c062921100c143df"]

place_names = ['PhanDinhGiot-PhanThucDuyen','QuangTrung-LeVanTho','PVD-Phan van Tri',
                'PhanDangLuu-TrangLong','Nga4bon xa','VoVanKiet-HoNgocLam','DH-NongLam'             #DinhBoLinh-BachDang(5)old
                ,"NamKyKhoiNghia-TranQuocToan","TruongChinh-AuCo","TruongChinh-TanKyTanQuy",
                "LeVanSy-HoangVanThu","TruongSon-GaNoi","NgVanCu-TranHungDao",
                "NgHuuCanh-NgBinhKhiem","MaiChiTho-LuongDC","XaLoHN-TayHoa"]
places_rm = ['P+P','Q+L','PD+P','PL+NT','Nga4','DB+B','DH+N',"NK+T","TC+AC","TC+TK","LVS+HT","TS+QN","NVC+THD","NHC+NBK","MCT+LDC","XLHN+TH"]
class_names = ['Vang','Thua thot','Trung binh','Dong','Ket xe']
class_names_en = ['empty','sparse','medium','crowded','traffic jam']

image_width = 64
image_height = 128
num_images = 4646
# Parameters
learning_rate = 0.00001
num_steps = 2000
batch_size = 150    # numbers of training images for a step
display_step = 100

# Network Parameters
n_hidden_1 = 768 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 256 # 3nd layer number of neurons
num_input = 24576 # MNIST data input (img shape: 64*128)
num_classes = 5 # MNIST total classes (0-4)

#load_weight_path = re.sub(["n_hidden_1","n_hidden_2","n_hidden_3"],[n_hidden_1,n_hidden_2,n_hidden_3],load_weight_path)

# Define the neural network. To use eager API and tf.layers API together,
# we must instantiate a tfe.Network c2940lass as follow:
class MNISTModel(tf.keras.Model):
  def __init__(self):
    super(MNISTModel, self).__init__()
    self.dense1 = tf.keras.layers.Dense(units = n_hidden_1,activation = tf.nn.relu)
    self.dense2 = tf.keras.layers.Dense(units = n_hidden_2,activation = tf.nn.relu)
    self.dense3 = tf.keras.layers.Dense(units = n_hidden_3,activation = tf.nn.relu)
    self.denseout = tf.keras.layers.Dense(units = num_classes)

  def call(self, input):
    """Run the model."""
    result = self.dense1(input)
    result = self.dense2(result)
    result = self.dense3(result)
    result = self.denseout(result)  # reuse variables from dense2 layer
    return result

def modelLearn():
    model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
        keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
        keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
    return model





'''
class NeuralNet(tfe.Network):
    def __init__(self):
        # Define each layer
        super(NeuralNet, self).__init__()
        # Hidden fully connected layer with 256 neurons
        self.layer1 = self.track_layer(
            tf.layers.Dense(n_hidden_1, activation=tf.nn.relu))
        # Hidden fully connected layer with 256 neurons
        self.layer2 = self.track_layer(
            tf.layers.Dense(n_hidden_2, activation=tf.nn.relu))
        # Output fully connected layer with a neuron for each class
        self.out_layer = self.track_layer(tf.layers.Dense(num_classes))

    def call(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        return self.out_layer(x)
neural_net = NeuralNet()
'''




# Cross-Entropy loss function
def loss_fn(inference_fn, inputs, labels):
    # Using sparse_softmax cross entropy
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=inference_fn(inputs), labels=labels))

# Calculate accuracy
def accuracy_fn(inference_fn, inputs, labels):
    prediction = tf.nn.softmax(inference_fn(inputs))
    #print(prediction)
    correct_pred = tf.equal(tf.argmax(prediction, 1), labels)
    return tf.reduce_mean(tf.cast(correct_pred, tf.float32))

def get_prediction(inference_fn, inputs, labels):
    return tf.nn.softmax(inference_fn(inputs))

'''
def saveModel():
    export_dir = "../mod"
    builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
    with tf.Session(graph=tf.Graph()) as sess:
      builder.add_meta_graph_and_variables(sess,[tf.saved_model.tag_constants.TRAINING])
    with tf.Session(graph=tf.Graph()) as sess:
      builder.add_meta_graph(["bar-tag", "baz-tag"])
    builder.save()

def loadModel():
    export_dir = "../mod"
    with tf.Session(graph=tf.Graph()) as sess:
      tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.TRAINING], export_dir)
'''
#loadModel()

def get_traindata():
    train_images = np.loadtxt(TRAIN_DATA_PATH)
    #print(train_images.shape) 989 images (989, 64, 384)
    print(train_images.shape)
    num_ima = int(train_images.shape[0]/image_width)
    total_size = int(train_images.shape[1]*image_width) # 64*384 = 24576
    train_images = train_images.reshape((num_ima,total_size))
    #print(train_images.shape)
    train_labels = np.loadtxt(TRAIN_LABES_PATH)
    train_labels = train_labels.astype(int)
    return train_images,train_labels

def get_testdata():
    test_image = np.loadtxt(TEST_DATA_PATH)
    num_ima = int(test_image.shape[0]/image_width)
    total_size = int(test_image.shape[1]*image_width) # 64*384 = 24576
    testX = test_image.reshape((num_ima,total_size))
    test_labels = np.loadtxt(TEST_LABES_PAHT)
    testY = test_labels.astype(int)
    return testX, testY

def get_predictData():
    test_images = np.loadtxt(TEST_DATA_PATH)
    num_ima = int(test_images.shape[0]/image_width)
    test_images = test_images.reshape((num_ima,image_width,test_images.shape[1])).astype(int)
    test_labels = np.loadtxt(TEST_LABES_PAHT)
    test_labels = test_labels.astype(int)
    return test_images, test_labels

# Training
def training():
    rate = [0.00001,0.0001]
    batch = [170,150,140]
    test_acuracy = 0;
    br = False
    for bat in batch:
        batch_size = bat
        for value_rate in rate:
            learning_rate = value_rate
            model = MNISTModel()

            train_images,train_labels = get_traindata()

            dataset = tf.data.Dataset.from_tensor_slices(
                (train_images,train_labels))
            dataset = dataset.repeat().batch(batch_size).prefetch(batch_size)
            dataset_iter = tfe.Iterator(dataset)
            # SGD Optimizer
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            # Compute gradients
            grad = tfe.implicit_gradients(loss_fn)
            average_loss = 0.
            average_acc = 0.
            for step in range(num_steps):

                # Iterate through the dataset
                d = dataset_iter.next()

                # Images
                x_batch = d[0]
                # Labels
                y_batch = tf.cast(d[1], dtype=tf.int64)

                # Compute the batch loss
                batch_loss = loss_fn(model, x_batch, y_batch)
                average_loss += batch_loss
                # Compute the batch accuracy
                batch_accuracy = accuracy_fn(model, x_batch, y_batch)
                average_acc += batch_accuracy

                if step == 0:
                    # Display the initial cost, before optimizing
                    print("Initial loss= {:.9f}".format(average_loss))

                # Update the variables following gradients info
                optimizer.apply_gradients(grad(model, x_batch, y_batch))

                # Display info
                if (step + 1) % display_step == 0 or step == 0:
                    if step > 0:
                        average_loss /= display_step
                        average_acc /= display_step
                    print("Step:", '%04d' % (step + 1), " loss=",
                          "{:.9f}".format(average_loss), " accuracy=",
                          "{:.4f}".format(average_acc))
                    if(average_acc>=0.90):
                        load_weight_path = TRAIN_WEIGHT_PATH+str(batch_size)+"+"+str(learning_rate)+"#"+str(n_hidden_1)+"-"+str(n_hidden_2)+"-"+str(n_hidden_3)+"#"+str(int(average_acc*100))+".h5"
                        model.save_weights(load_weight_path)
                        # model.summary()
                    test_acuracy = average_acc
                    average_loss = 0.
                    average_acc = 0.
            if(test_acuracy > 0.95):
                br = True
        if(br):
            break

    #model.save_weights(TRAIN_WEIGHT_PATH)
    testX,testY = get_testdata()
    test_acc = accuracy_fn(model, testX, testY)
    print("Testset Accuracy: {:.4f}".format(test_acc))
    #model.summary()

#training()
def plot_image(i,predictions_array,true_label,img):
    predictions_array,true_label,img = predictions_array[i],true_label[i],img[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(img.reshape(image_width,image_height,3),cmap=plt.cm.binary)
    predicted_label = np.argmax(predictions_array)
    if predicted_label  == true_label:
        color = 'blue'
    else:
        color = 'red'
    plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                    100*np.max(predictions_array),
                    class_names[int(true_label)]),
                    color = color)
def plot_value_array(i,predictions_array,true_label):
    predictions_array,true_label = predictions_array[i],true_label[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    thisplot = plt.bar(range(5),predictions_array, color = "#777777")
    plt.ylim = ([0,1])
    predicted_label = int(np.argmax(predictions_array))
    #print(predicted_label)
    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')

def testing():
    #load model weights
    model = modelLearn()
    model.load_weights(load_weight_path)
    model.summary()
    model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

    #load data to predict
    test_images,test_labels = get_predictData()
    #Evaluate accuracy
    test_loss, test_acc  = model.evaluate(test_images,test_labels)

    print('Test accuracy:',test_acc)

    predictions = model.predict(test_images)

    print(predictions)

    #Plot the first X test images, their predicted label, and  the true label
    # Color correct predictions in blue, incorrect predictions in red

    num_rows = 5
    num_cols = 3
    num_images = num_rows*num_cols
    plt.figure(figsize=(2*2*num_cols,2*num_rows))

    for i in range(num_images):
    	plt.subplot(num_rows,2*num_cols,2*i+1)
    	plot_image(i+10,predictions,test_labels,test_images)
    	plt.subplot(num_rows,2*num_cols,2*i+2)
    	plot_value_array(i+10,predictions,test_labels)
    plt.show()

def resize_crop(image,size):
    img_format = image.format
    img = image.copy()
    old_size = img.size
    left = (old_size[0] - size[0]) / 2
    top = (old_size[1] - size[1]) / 2
    right = (old_size[0] - left)
    bottom = old_size[1] - top
    rect = [int(math.ceil(x)) for x in (left,top,right,bottom)]
    left, top, right, bottom = rect
    crop = image.crop((left,top,right,bottom))
    crop.format = img_format
    return crop


def resize_cover(image,size):
    img_format = image.format
    img = image.copy()
    img_size = img.size
    ratio = max(size[0] / img_size[0],size[1] / img_size[1])
    new_size = [int(math.ceil(img_size[0] * ratio)),int(math.ceil(img_size[1]*ratio))]
    img = img.resize((new_size[0],new_size[1]),Image.LANCZOS)
    img = resize_crop(img,size)
    img.format = img_format
    return img

def read_image_data(url):
    try:
        file = BytesIO(urllib.request.urlopen(url).read())
        img = Image.open(file)
        img = resize_cover(img,[image_height,image_width])
        array = np.array(img).reshape(image_width,image_height*3)
        return array.reshape((1,-1))
    except Exception as e:
        return []


def download_image(URL,pathName):
    with open(pathName,'wb') as handle:
        response = requests.get(URL,stream = True)
        if not response.ok:
            print(response)
        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)

def showPrediction(place_index,array,prediction):
    try:
        int(np.argmax(prediction))
        fig = plt.figure()
        plt.title(place_names[place_index])
        plt.imshow(array.reshape(image_width,image_height,3))
        plt.colorbar()
        plt.grid(False)
        plt.xlabel("{} {:2.0f}%".format(class_names_en[int(np.argmax(prediction))],
                        100*np.max(prediction),
                        color = 'red'))
        #plt.show()
        fig.savefig(figureSave)
    except Exception as e:
        return 0

def savePredictData(fileName,data):
    try:
        path = DATA_PREDICTION_JSON_PATH + fileName+".txt"
        with open(path,'w') as fp:
            np.savetxt(path,data,fmt="%d")
        return 1
    except Exception as e:
        return 0

def printPrediction(place_index,prediction):
    print(place_names[place_index]+":------->"+"{} {:2.0f}%".format(class_names_en[int(np.argmax(prediction))],100*np.max(prediction)))

def showImagePredict(imagesArr,predictions):

    try:
        num_rows = 4
        num_cols = int(len(places_rm)/num_rows)
        num_images = len(imagesArr) # len(imagesArr)
        fig = plt.figure(figsize=(2*2*num_cols,2*num_rows))
        for i in range(num_images):
            plt.grid(False)
            plt.xticks([])
            plt.yticks([])
            plt.subplot(num_rows,num_cols,i+1)
            plt.title(place_names[i])
            if len(imagesArr[i])== 1 :
                plt.imshow(imagesArr[i].reshape(image_width,image_height,3),cmap=plt.cm.binary)
                plt.xlabel("{} {:2.0f}% ".format(class_names_en[int(np.argmax(predictions[i]))],100*np.max(predictions[i])))
            else:
                plt.xlabel("Could not load images")
        plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.5,wspace=0.35)
        plt.suptitle('Traffic condition at: {}'.format(time.strftime("%H:%M: %a, %d %b %Y ")), fontsize=16)
        plt.show()
        #fig.savefig(FIGURE_SAVE_PATH+time.strftime("%H:%M:%S%a")+".png")
        #plt.close('all')
    except Exception as e:
        print(e)
        return 0

def work_time():
    timeZone = int(time.strftime("%z"))
    hour = int(time.strftime("%H")) - (timeZone/100 -7)
    if(hour>=6 and hour <9):
        return True
    elif(hour >=10 and hour < 13):
        return True
    elif(hour >=15 and hour <20):
        return True
    else:
        return False

def predicting():
    #Connect to database

    cred = credentials.Certificate(FIREBASE_CERTIFICATE)
    default_app = firebase_admin.initialize_app(cred,{'databaseURL': FIREBASE_APP_URL})
    traffic_ref = db.reference(FIREBAE_REF_VALUE)
    #load model weights
    model1 = modelLearn()
    model1.load_weights(load_weight_g1)
    model2 = modelLearn()
    model2.load_weights(load_weight_g2)
    #model.summary()
    model1.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])
    model2.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])
    numRecycel = 0
    objectArr = []
    cameras = {}
    with open(IMAGES_JON_PATH) as data_file:
        cameras = json.load(data_file)
    while True:
        if(work_time()):
            try:
                print(" Time: "+time.strftime("%a, %d %b %Y %H:%M:%S"))
                place_index = 0
                imagesArr = []
                predictions = []
                for key in cameras:
                    url = cameras.get(key).get('link')
                    predict_image = read_image_data(url)
                    if(len(predict_image) > 0):
                        #data['predict']['place_index'] = place_index
                        imagesArr.append(predict_image)
                        #print(predict_image.shape)
                        #predict_image = predict_image.T
                        #print(predict_image.shape)
                        group = int(key.split("-")[1])
                        if(group == 1):
                            prediction = model1.predict(predict_image)
                        else:
                            prediction = model2.predict(predict_image)
                        predictions.append(prediction)
                        timeZone = int(time.strftime("%z"))
                        day =  int(time.strftime("%w"))
                        hour = int(time.strftime("%H")) - (timeZone/100 - 7)
                        minute = int(time.strftime("%M"))
                        # push data to firebase
                        traffic_ref.child(key).update({"val":int(np.argmax(prediction))+1})
                        print(key)
                        print({"val":int(np.argmax(prediction))+1})
                        #print(int(np.argmax(prediction))+1)
                        #write data to learn
                        export = [place_index,day,hour,minute,int(np.argmax(prediction))+1]
                        objectArr.append(export)
                        #printPrediction(place_index,prediction)
                        group = key.split("-")[1]
                        camera_id = key.split("-")[2]
                        save_image_patth = PREFIX_SAVE_IMAGE_PATH
                        if(group == "1"):
                            if(hour > 17 and minute > 30):
                                save_image_patth =save_image_patth+"group1_night_predict/"
                            else:
                                save_image_patth =save_image_patth+"group1_day_predict/"
                        else:
                            if(hour > 17 and minute > 30):
                                save_image_patth =save_image_patth+"group2_night_predict/"
                            else:
                                save_image_patth =save_image_patth+"group2_day_predict/"
                        save_image_patth = save_image_patth+"%s:%d:%d:%d:%d.jpg"%(str(camera_id),day,hour,minute,int(np.argmax(prediction))+1)
                        download_image(url,save_image_patth)
                        #showPrediction(place_index,predict_image,prediction)
                        if(place_index == (len(cameras)-1)):
                            numRecycel +=1
                            #showImagePredict(imagesArr,predictions)
                            time.sleep(90)
                            place_index = 0
                    else:
                        imagesArr.append([])
                        predictions.append([])
                        traffic_ref.child(key).update({"val":0})
                        print(url)
                        print("--")
                    place_index += 1
                if(numRecycel % 5 == 0):
                    savePredictData(str(time.time()),np.asarray(objectArr))
                    print("Saved data!")
                    objectArr = []
            except Exception as e:
                print(" Time: "+time.strftime("%a, %d %b %Y %H:%M:%S"))
                print(e)

def main():
    #training()
    predicting()

if __name__ == '__main__':
    main()
