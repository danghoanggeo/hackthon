import numpy as np
import matplotlib.pyplot as plt


PREDICT_FILE_PATH_TRAIN = '../temp/correct_predict.txt'

data = np.loadtxt(PREDICT_FILE_PATH_TRAIN)
place = data[:,0]
evaluate = data[:,4]
days = data[:,1]
hours = data[:,2]
minutes = data[:,3]
day_s = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
sun_t = []
sun_e = []
mon_t = []
mon_e = []
tue_t = []
tue_e = []
wed_t = []
wed_e = []
thu_t = []
thu_e = []
fri_t = []
fri_e = []
sat_t = []
sat_e = []


j = 0
for i in days:
    if(int(place[j]) == 5):
        time = int(hours[j])-2 + int(minutes[j])/60
        if(int(i) == 0):
            sun_t.append(time)
            sun_e.append(int(evaluate[j])+1)
        elif(int(i)==1):
            mon_t.append(time)
            mon_e.append(int(evaluate[j])+1)
        elif(int(i)==2):
            tue_t.append(time)
            tue_e.append(int(evaluate[j])+1)
        elif(int(i)==3):
            wed_t.append(time)
            wed_e.append(int(evaluate[j])+1)
        elif(int(i)==4):
            thu_t.append(time)
            thu_e.append(int(evaluate[j])+1)
        elif(int(i)==5):
            fri_t.append(time)
            fri_e.append(int(evaluate[j])+1)
        else:
            sat_t.append(time)
            sat_e.append(int(evaluate[j])+1)
    j = j+1

def sortArray(arr1,arr2):
    sunt = np.array([arr1, arr2])
    arr = sunt.T
    arr = arr[arr[:,0].argsort()]
    arr1 = arr[:,0]
    arr2 = arr[:,1]
    return arr1,arr2

sun_t, sun_e = sortArray(sun_t,sun_e)
mon_t, mon_e = sortArray(mon_t,mon_e)
tue_t, tue_e = sortArray(tue_t,tue_e)
wed_t, wed_e = sortArray(wed_t,wed_e)
thu_t, thu_e = sortArray(thu_t,thu_e)
fri_t, fri_e = sortArray(fri_t,fri_e)
sat_t, sat_e = sortArray(sat_t,sat_e)

plt.figure(6)
plt.subplot(331)
plt.title("Sunday")
plt.plot(sun_t, sun_e, 'b')
plt.subplot(332)
plt.title("Monday")
plt.plot(mon_t, mon_e, 'g')
plt.subplot(333)
plt.title("Tueday")
plt.plot(tue_t, tue_e, 'r')
plt.subplot(334)
plt.title("Wednesday")
plt.plot(wed_t, wed_e, 'k')
plt.subplot(335)
plt.title("Thurday")
plt.plot(thu_t, thu_e, 'g')
plt.subplot(336)
plt.title("Friday")
plt.plot(fri_t, fri_e, 'b')
plt.subplot(337)
plt.title("Satuday")
plt.plot(sat_t, sat_e, 'b')
plt.suptitle("DinhBoLinh-BachDang")
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.5,wspace=0.35)
#plt.plot(sun_t, sun_e, 'bo', mon_t, mon_e, 'ko',tue_t,tue_e,'ro',wed_t,wed_e,'go',thu_t,thu_e,'yo',fri_t,fri_e,'b+',sat_t,sat_e,'r*')
plt.show()
